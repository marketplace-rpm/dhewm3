# Information / Информация

SPEC-файл для создания RPM-пакета **dhewm3**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/dhewm3`.
2. Установить пакет: `dnf install dhewm3`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)